---
title:
layout: default
category: none
order: 1
permalink:
summary: Front Page
lastupdate: 20-12-2018
---


# Header One

## Header Two

### Header Three
#### Header Four
##### Header 05

`object~`

## Header:

**bold**:

**bold2:**


This is the *normal* **paragraph** text ~~here~~.


* First Level
	* Second Level
		* Third Level

--

1. first
	2. Second

> This is a block quote

	This is a code block


****


| Column 01| Column 02 | Column 03|
|-|-|-|
|Content 01| Content 02 | Content 03|
|||Joined Cell|
|Joined here|More||

### Images
![Ableton Live](https://i.ytimg.com/vi/mpYbSv-hHuE/hqdefault.jpg)


An inline link: <http://www.bbc.co.uk>

A referenced [link](www.stv.co.uk)


This is a statement that should be attributed to
it's source[p. 23][#Author].

[#Author]: Author. *Book Title*.  Publisher, Year.


An inline footnote[^inline footnote content]

A referenced footnote [^pizza]

[^pizza]: Referenced footnote content


[?chorus] is an effect 

[?chorus]: a mentalist





 