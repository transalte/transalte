---
title: Max Articles
layout: default
category: None
order: 1
permalink: /max/
summary: Content relating to Max / Max For Live
lastupdate: 01.07.19
---


## Fundamentals of Max

<ul>{% for doc in site.docs %}
{% if doc.category == "Max" %}
<li><a href="{{ doc.url }}">{{ doc.title }}</a></li>
          {% endif %}
        {% endfor %}</ul>

## Audio Effects
<ul>{% for doc in site.docs %}
{% if doc.category == "asp_lessons" %}
<li><a href="{{ doc.url }}">{{ doc.title }}</a></li>
          {% endif %}
        {% endfor %}</ul>


## Instruments
<ul>{% for doc in site.docs %}
{% if doc.category == "aasp_lessons" %}
<li><a href="{{ doc.url }}">{{ doc.title }}</a></li>
          {% endif %}
      {% endfor %}</ul>


## MIDI Effects

